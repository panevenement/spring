package com.example.demo.controller;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String index(){
    return "Greeting from Spring Boot!";
    }

    @GetMapping("/number")
    public String getNumber(@RequestParam int id){
        return "your id: " + id;
    }

    @GetMapping("/xxx/{name}/{age}")
    public String xxxAccess(@PathVariable String name, @PathVariable int age){
        return "Hello " + name + ", you are " + (age >= 18 ? "adult." : "too young.");
    }

}
