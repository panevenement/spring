package com.example.demo.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class Calculator {

    @GetMapping("/add/{x}/{y}")
    public double add(@PathVariable double x, @PathVariable double y) {
        return x + y;
    }

    @GetMapping("/multiply/{x}/{y}")
    public double multiply(@PathVariable double x, @PathVariable double y) {
        return x * y;
    }

    @GetMapping("/divide/{x}/{y}")
    public double divide(@PathVariable double x, @PathVariable double y) {
        if (x == 0 || y == 0) {
            return Double.parseDouble("Nie dziel przez 0");
        } else {
            return x / y;
        }
    }

    @GetMapping("/substraction/{x}/{y}")
    public double substraction(@PathVariable double x, @PathVariable double y) {
        return x - y;
    }

    @GetMapping("/{x}/{y}")
    public double calculate(@PathVariable double x,@PathVariable double y, @RequestParam String operation) {
        switch (operation) {
            case "multiply":
                return x * y;
            case "add":
                return x + y;
            case "divide":
                if (x == 0 || y == 0) {
                    return Double.parseDouble("nie dziel przez 0");
                } else {
                    return x / y;
                }

            case "substraction":
                return x - y;

            default:
                return Double.parseDouble("Błędne dane");

        }
    }
}
